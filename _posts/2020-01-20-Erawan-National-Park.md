---
layout: dienos
tags: [Erawan National Park, Kanchanaburi, Thailand]
categories: [SEA]
permalink: /:categories/:year/:month/:day/:title/
diena: 8
dienu_sk: 0
nueita_snd: 15
nueita_viso: 122
temperatura: 32
nukeliauta: 10889
foto_sk: 29
komentarai_id: []
komentarai: []
formatai_id: [24,27]
formatai: [mov,mov]
zemelapis: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1061788.1831269134!2d98.49582262373276!3d14.475773016741288!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e476a41fbc7b15%3A0x54ee9cba8062c97a!2sErawan%20National%20Park!5e0!3m2!1sen!2sth!4v1579795853636!5m2!1sen!2sth"
---
