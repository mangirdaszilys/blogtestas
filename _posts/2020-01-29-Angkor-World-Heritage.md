---
layout: dienos
tags: [Angkor World Heritage, Siem Reap, Cambodia]
categories: [SEA]
permalink: /:categories/:year/:month/:day/:title/
diena: 17
dienu_sk: 0
nueita_snd: 21
nueita_viso:  220
temperatura: 33
nukeliauta: 11635
foto_sk: 79
komentarai_id: []
komentarai: []
formatai_id: []
formatai: []
zemelapis: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d105582.87979051626!2d103.81109627174423!3d13.417118530031457!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3110168aea9a272d%3A0x3eaba81157b0418d!2sAngkor%20Wat!5e0!3m2!1sen!2skh!4v1580907002247!5m2!1sen!2skh"
---
